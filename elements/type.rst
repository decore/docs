### Типографика

+ http://typejs.org/ отступы между буквами
+ https://eager.io/showcase/SmartUnderline/ true подчеркивание
+ https://github.com/Gigacore/four-shadows тень зависит от вермени суток
+ http://zencode.in/lining.js/#demo-3 :nth-line(), content: attr(index) '.'
+ https://github.com/leeoniya/preCode.js pre
+ http://gabinaureche.com/TheaterJS/ автоматический набор текста, имитирующий поведение человека
