background
---------------

+ http://thepatternlibrary.com/ бесшовные текстуры
+ http://tympanus.net/Development/AnimatedHeaderBackgrounds/index4.html
+ http://formstone.it/components/Wallpaper/demo/index.html 
+ фоторама
+ https://github.com/mrdoob/texgen.js процедурный генератор текстур
+ http://riccardoscalco.github.io/textures/ svg текстуры
+ http://qrohlf.com/trianglify-generator/ генератор фона с треугольниками

pages layouts
---------------
+ http://tympanus.net/Tutorials/SlidingHeaderLayout/layout-multi.html поворот страницы

### фиксирование страницы
+  http://codyhouse.co/demo/fixed-background-effect/index.html#0