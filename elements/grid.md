### Обычные сетки

+ http://flexboxgrid.com/
+ http://neat.bourbon.io/ 
+ http://jeet.gs/ 

### Сетки по-типу масонри masonry masonri

+ http://packery.metafizzy.co/
+ http://masonjs.com/
+ http://ed-lea.github.io/jquery-collagePlus/
+ http://collageplus.edlea.com/ только картинки, разные эффекты
+ http://salvattore.com/
+ http://akoenig.github.io/angular-deckgrid/#/ на angular
+ http://brunjo.github.io/rowGrid.js/ 
+ http://demosthenes.info/blog/844/Easy-Masonry-Layout-With-Flexbox на flexbox
+ https://strml.github.io/react-grid-layout/examples/0-showcase.html React-Grid-Layout masonry